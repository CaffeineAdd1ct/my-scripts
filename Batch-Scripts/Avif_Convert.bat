@echo off
setlocal enabledelayedexpansion

if not exist avif mkdir avif

for %%f in (*.jpg *.jpeg *.png *.bmp) do (
    set "filename=%%~nf"
    avifenc.exe -q 80 "%%f" ".\avif\!filename!.avif"
)

echo Conversion complete!
pause