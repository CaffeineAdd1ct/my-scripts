@echo off
setlocal

rem Define source and destination directories
set "SOURCE_DIR=%~dp0"
set "OUTPUT_DIR=%SOURCE_DIR%output"

rem Create output directory if it doesn't exist
if not exist "%OUTPUT_DIR%" mkdir "%OUTPUT_DIR%"

rem Get a list of supported image files in the current directory
for %%i in (*.jpg *.jpeg *.png *.tiff *.bmp) do (
    rem Convert each file to .jxl format and save in output directory
    echo Converting "%%i"...
    cjxl.exe "%%i" "%OUTPUT_DIR%\%%~ni.jxl"
)

echo Conversion complete.
pause
endlocal