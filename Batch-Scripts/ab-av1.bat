@echo off
setlocal enabledelayedexpansion

REM Define the list of extensions to process
set "extensions=.avi .mp4 .mkv"

REM Get the current directory
set "currentDir=%~dp0"

REM Define the path to ab-av1.exe
set "abAv1Path=C:\Users\windows\scoop\apps\ab-av1\0.9.1\ab-av1.exe"

REM Check if ab-av1.exe exists
if not exist "%abAv1Path%" (
    echo ab-av1.exe not found at "%abAv1Path%".
    exit /b 1
)

REM Loop through each file in the current directory
for %%f in ("%currentDir%*") do (
    REM Extract the file extension and name
    set "fullPath=%%f"
    set "fileExt=%%~xf"
    set "fileName=%%~nf"
    set "fileDir=%%~dpf"

    REM Skip directories
    if exist "%%f\" (
        echo Skipping directory: "%%f"
        continue
    )

    REM Exclude files with .av1.mkv extension
    if /i "!fileExt!"==".av1.mkv" (
        echo Skipping already encoded file: "%%f"
        continue
    )

    REM Exclude audio files with .mka or .opus extensions
    if /i "!fileExt!"==".mka" (
        echo Skipping audio file: "%%f"
        continue
    )
    if /i "!fileExt!"==".opus" (
        echo Skipping audio file: "%%f"
        continue
    )

    REM Check if the file extension is in the list of extensions to process
    if "!extensions!" neq "!extensions:%fileExt%=!" (
        REM Process the file
        echo Processing "!fullPath!"
        call "%abAv1Path%" auto-encode --input "!fullPath!"
        if !errorlevel! neq 0 (
            echo Error processing "!fullPath!"
        )
    ) else (
        echo Skipping file with unsupported extension: "%%f"
    )
)

echo All files processed.
endlocal