@echo off
setlocal enabledelayedexpansion

:: Get the current directory
set "CURRENT_DIR=%cd%"

:: List all directories in the current directory
for /d %%D in ("%CURRENT_DIR%\*") do (
    :: Move all files from the subdirectory to the current directory
    for %%F in ("%%D\*") do (
        if exist "%%F" (
            move "%%F" "%CURRENT_DIR%"
        )
    )
)

:: Optionally, remove the empty subdirectories
for /d %%D in ("%CURRENT_DIR%\*") do (
    if exist "%%D" (
        rmdir "%%D" 2>nul
    )
)

echo Files have been moved to the original directory.