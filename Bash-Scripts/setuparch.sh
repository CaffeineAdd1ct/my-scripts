#!/bin/bash

# Sync package repositories and update the system
sudo pacman -Syu --noconfirm

# Install reflector for updating mirrorlist
sudo pacman -S --needed reflector --noconfirm

# Use reflector to generate a new mirrorlist with the fastest servers in the US
sudo reflector --verbose --country 'United States' --latest 50 --sort rate --save /etc/pacman.d/mirrorlist

# Update the mirrorlist
sudo pacman -Syy --noconfirm

# Install go as a prerequisite
sudo pacman -S --needed go --noconfirm

# Create a new user account named "arch" with a random password
random_password=$(date +%s | sha256sum | base64 | head -c 12 ; echo)
sudo useradd -m -G wheel -s /bin/bash arch
echo -e "$random_password\n$random_password" | sudo passwd arch --quiet

# Allow passwordless sudo for the "arch" user
echo 'arch ALL=(ALL) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/arch

# Log in as the "arch" user and execute subsequent commands
su -l arch << EOF

# Update the system
sudo pacman -Syu --noconfirm

# Install fish
sudo pacman -S fish --noconfirm

# Clone yay repository and install it
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ..
rm -rf yay

# Install pacseek using yay
yay -S --noconfirm pacseek

# Change default shell to fish
chsh -s /usr/bin/fish

# Download pfetch
git clone https://github.com/dylanaraps/pfetch.git
cd pfetch
sudo make install
cd ..
rm -rf pfetch

# Append fish command to .bashrc
echo "fish" >> ~/.bashrc

# Configure fish greeting and launch pfetch
echo "set -U fish_greeting '🐟'
pfetch" >> ~/.config/fish/config.fish

# Additional script for Chaotic-AUR setup
chaotic_script="# Install the primary key
sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key 3056513887B78AEB

# Install the Chaotic-AUR keyring and mirrorlist
sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

# Add Chaotic-AUR repository to /etc/pacman.conf
echo -e \"\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist\" | sudo tee -a /etc/pacman.conf

# Update package lists
sudo pacman -Sy --noconfirm

echo \"Chaotic-AUR setup completed!\"
"

# Append Chaotic-AUR script to .bashrc
echo "$chaotic_script" >> ~/.bashrc

EOF

# Output the random password
echo "Random password for 'arch' user: $random_password"

# Remove passwordless sudo access
sudo rm /etc/sudoers.d/arch

# Start fish shell
fish
