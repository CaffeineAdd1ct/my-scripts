#!/bin/bash

# Function to display script usage information
show_usage() {
    echo "Usage: ./convert_media.sh [OPTIONS] input_file1.png input_file2.jpg input_file2.mp4 ..."
    echo
  echo "Options:"
  echo "  -h, --help    Display this help message and exit"
}

if [ $# -eq 0 ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  show_usage
  exit 0
fi

# Check if required dependencies are installed
if ! command -v ffmpeg >/dev/null 2>&1 || ! command -v exiftool >/dev/null 2>&1; then
  echo "Error: This script requires 'ffmpeg' and 'exiftool' to be installed."
  exit 1
fi

# Process each input file
for input_file in "$@"; do
  extension="${input_file##*.}"

  if [ "$extension" == "png" ] || [ "$extension" == "jpg" ] || [ "$extension" == "jpeg" ]; then
    # Convert image file to JPG
        output_file=${input_file%.*}.jpg
    tmp_dir=$(mktemp -d)
        tmp_file=${tmp_dir}/converted.jpg
        ffmpeg -y -i "$input_file" -preset slow -crf 18 -pix_fmt yuv420p -movflags +faststart "$tmp_file" > /dev/null 2>&1
        exiftool -all= "$tmp_file" > /dev/null 2>&1
    # Generate a random number and rename the file
    random_number=$(shuf -i 1000000000000000-9999999999999999 -n 1)
        output_file=${random_number}.jpg
        mv "$tmp_file" "$output_file"
    rm -rf "$tmp_dir"

  elif [ "$extension" == "mp4" ] || [ "$extension" == "gif" ]; then
        # Convert video or GIF to MP4
        output_file=${input_file%.*}_converted.mp4
        ffmpeg -y -i "$input_file" -c:v libx264 -crf 23 -preset slow -c:a aac -b:a 192k -pix_fmt yuv420p -movflags +faststart "$output_file" > /dev/null 2>&1
        exiftool -all= "$output_file" > /dev/null 2>&1
    # Generate a random number and rename the file
    random_number=$(shuf -i 1000000000000000-9999999999999999 -n 1)
        output_file=${random_number}.mp4
        mv "$output_file" "${input_file%.*}_original"
        # Remove temporary files
        rm -rf "${input_file%.*}"_converted.mp4
  else
    echo "Unsupported file format for file: $input_file. Only PNG, JPG, JPEG, MP4, and GIF files are supported."
  fi
done

# Clean up _original files if necessary
find . -type f -name "*.mp4_original" -delete

echo "Done."