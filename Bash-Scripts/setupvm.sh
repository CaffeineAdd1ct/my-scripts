#!/bin/bash

# Update the system
sudo pacman -Syu

# Install QEMU and KVM
sudo pacman -S qemu libvirt virt-manager ebtables dnsmasq bridge-utils openbsd-netcat

# Enable and start libvirtd service
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service

# Add user to libvirt group
sudo usermod -aG libvirt $(whoami)

# Print message to reboot for changes to take effect
echo "Installation complete. Please reboot your system to ensure all changes take effect."
