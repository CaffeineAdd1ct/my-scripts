#!/bin/bash

# Step 1: Edit the PolicyKit configuration file
sudo tee /etc/polkit-1/rules.d/50-udisks2.rules > /dev/null <<EOT
polkit.addRule(function(action, subject) {
    if ((action.id == "org.freedesktop.udisks2.filesystem-mount-system" ||
         action.id == "org.freedesktop.udisks2.filesystem-mount") &&
        subject.isInGroup("your_username")) {
        return polkit.Result.YES;
    }
});
EOT

# Step 2: Restart the PolicyKit service
sudo systemctl restart polkit.service

echo "User mount permission enabled successfully!"
