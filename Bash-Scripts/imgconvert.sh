#!/bin/bash

# Function to display usage instructions and options
help() {
    echo "Usage: $0 [OPTIONS] input_directory output_directory"
    echo "Script name: imgconvert.sh"
    echo ""
    echo "Options:"
    echo "  -e, --encode FORMAT        Encode to FORMAT (jxl or avif)"
    echo "  -d, --decode FORMAT        Decode from FORMAT (jxl or avif)"
    echo "  -o, --output OUTPUT_FORMAT Output format for decoding (png or jpg), default: png"
    echo "  -q, --quality QUALITY      Set encoding quality (0-100), default: 80"
    echo "  -l, --lossless             Use lossless encoding (only with -e)"
    echo "  -a, --auto-quality         Enable automated quality control for encoding"
    echo "  --ssim-threshold VALUE     Set SSIM threshold for auto-quality (default: 0.99)"
    echo "  -v, --verbose              Show detailed processing info"
    echo "  -h, --help                 Display this help message"
    echo ""
    echo "Examples:"
    echo "  Encode to AVIF with quality 85: $0 -e avif -q 85 images/ output/"
    echo "  Decode JXL to JPG: $0 -d jxl -o jpg images/ output/"
    echo "  Encode to AVIF with auto-quality: $0 -e avif -a images/ output/"
    echo "  Encode to JXL with auto-quality and SSIM 0.95: $0 -e jxl -a --ssim-threshold 0.95 images/ output/"
    echo ""
    echo "Note: Existing files in output_directory will be overwritten."
}

# Initialize default variables
encode_format=""          # Format to encode to (jxl or avif)
decode_format=""          # Format to decode from (jxl or avif)
output_format=""          # Output format for decoding (png or jpg)
quality=80                # Default encoding quality (0-100)
lossless=false            # Flag for lossless encoding
auto_quality=false        # Flag for automated quality control
ssim_threshold=0.99       # Default SSIM threshold for auto-quality
verbose=false             # Flag for verbose output
processed=false           # Tracks if any files were processed

# Parse command-line arguments
while [ "$#" -gt 0 ]; do
    case "$1" in
        -e | --encode )
            shift
            # Ensure a format is provided after -e
            if [ -z "$1" ] || [[ "$1" == -* ]]; then
                echo "Error: -e|--encode requires a format (jxl or avif)"
                help
                exit 1
            fi
            encode_format="$1"
            shift
            ;;
        -d | --decode )
            shift
            # Ensure a format is provided after -d
            if [ -z "$1" ] || [[ "$1" == -* ]]; then
                echo "Error: -d|--decode requires a format (jxl or avif)"
                help
                exit 1
            fi
            decode_format="$1"
            shift
            ;;
        -o | --output )
            shift
            # Ensure an output format is provided after -o
            if [ -z "$1" ] || [[ "$1" == -* ]]; then
                echo "Error: -o|--output requires a format (png or jpg)"
                help
                exit 1
            fi
            output_format="$1"
            shift
            ;;
        -q | --quality )
            shift
            # Ensure a quality value is provided after -q
            if [ -z "$1" ] || [[ "$1" == -* ]]; then
                echo "Error: -q|--quality requires a value (0-100)"
                help
                exit 1
            fi
            quality="$1"
            shift
            ;;
        -l | --lossless )
            lossless=true
            shift
            ;;
        -a | --auto-quality )
            auto_quality=true
            shift
            ;;
        --ssim-threshold )
            shift
            # Ensure a threshold value is provided
            if [ -z "$1" ] || [[ "$1" == -* ]]; then
                echo "Error: --ssim-threshold requires a value"
                help
                exit 1
            fi
            ssim_threshold="$1"
            shift
            ;;
        -v | --verbose )
            verbose=true
            shift
            ;;
        -h | --help )
            help
            exit 0
            ;;
        -* )
            echo "Error: Unknown option: $1"
            help
            exit 1
            ;;
        * )
            break  # Remaining arguments are directories
            ;;
    esac
done

# Validate that exactly two directories are provided
if [ "$#" -ne 2 ]; then
    echo "Error: Exactly two directories required: input_directory output_directory"
    help
    exit 1
fi
input_dir="$1"
output_dir="$2"

# Check if input directory exists and is readable
if [ ! -d "$input_dir" ] || [ ! -r "$input_dir" ]; then
    echo "Error: Input directory does not exist or is not readable: $input_dir"
    exit 1
fi
# Create output directory if it doesn’t exist
mkdir -p "$output_dir" || { echo "Error: Cannot create output directory: $output_dir"; exit 1; }
# Check if output directory is writable
if [ ! -w "$output_dir" ]; then
    echo "Error: Output directory is not writable: $output_dir"
    exit 1
fi

# Ensure either encode or decode mode is specified, but not both
if [ -z "$encode_format" ] && [ -z "$decode_format" ]; then
    echo "Error: Specify either -e/--encode or -d/--decode"
    help
    exit 1
fi
if [ -n "$encode_format" ] && [ -n "$decode_format" ]; then
    echo "Error: Cannot use both -e/--encode and -d/--decode"
    help
    exit 1
fi
# Lossless is only valid with encoding
if [ "$lossless" = true ] && [ -n "$decode_format" ]; then
    echo "Error: -l/--lossless is only valid with -e/--encode"
    help
    exit 1
fi
# Auto-quality requires encoding and specific tools
if [ "$auto_quality" = true ]; then
    if [ -z "$encode_format" ]; then
        echo "Error: -a/--auto-quality is only valid with -e/--encode"
        help
        exit 1
    fi
    command -v ffmpeg-quality-metrics >/dev/null 2>&1 || { echo "Error: ffmpeg-quality-metrics not found"; exit 1; }
    command -v jq >/dev/null 2>&1 || { echo "Error: jq not found"; exit 1; }
fi

# Convert quality to AVIF quantizer values (min and max)
get_avif_quantizers() {
    local q=$1
    local min_q=$(( (100 - q) * 63 / 100 ))  # Map 0-100 quality to 63-0 quantizer range
    local max_q=$min_q                        # Use same min and max for simplicity
    echo "--min $min_q --max $max_q"
}

# Calculate SSIM between compressed and original image
calculate_ssim() {
    local compressed="$1"
    local original="$2"
    local metrics_output=$(mktemp)  # Temporary file for metrics output
    # Run SSIM calculation and output as JSON
    ffmpeg-quality-metrics "$compressed" "$original" -m ssim -of json > "$metrics_output" 2>/dev/null
    local ssim=$(jq '.global.ssim.ssim_avg.average' "$metrics_output")  # Extract average SSIM
    rm "$metrics_output"  # Clean up temp file
    [ -z "$ssim" ] || [ "$ssim" = "null" ] && ssim=0  # Default to 0 if calculation fails
    echo "$ssim"
}

# Binary search to find optimal quality based on SSIM threshold
find_optimal_quality() {
    local original="$1"
    local encode_format="$2"
    local temp_dir="$3"
    local low=0
    local high=100
    local optimal_quality=100

    while [ $low -le $high ]; do
        local mid=$(( (low + high) / 2 ))
        local temp_compressed="$temp_dir/temp_compressed.$encode_format"
        # Encode with current quality
        case "$encode_format" in
            jxl )
                cjxl -q $mid "$original" "$temp_compressed" > /dev/null 2>&1 || continue
                ;;
            avif )
                local quantizer=$(( (100 - mid) * 63 / 100 ))
                avifenc --min $quantizer --max $quantizer "$original" "$temp_compressed" > /dev/null 2>&1 || continue
                ;;
        esac
        local ssim=$(calculate_ssim "$temp_compressed" "$original")
        [ "$verbose" = true ] && echo "Testing quality $mid for $original: SSIM = $ssim"
        # Adjust search range based on SSIM threshold
        if [ $(echo "$ssim >= $ssim_threshold" | bc -l) -eq 1 ]; then
            optimal_quality=$mid
            high=$((mid - 1))
        else
            low=$((mid + 1))
        fi
    done
    echo "$optimal_quality"
}

# Encoding logic
if [ -n "$encode_format" ]; then
    # Set encoder and supported extensions based on format
    case "$encode_format" in
        jxl )
            encoder="cjxl"
            output_ext=".jxl"
            supported_exts=("png" "jpg" "jpeg")
            ;;
        avif )
            encoder="avifenc"
            output_ext=".avif"
            supported_exts=("png" "jpg" "jpeg")
            ;;
        * )
            echo "Error: Unsupported encode format: $encode_format (use jxl or avif)"
            exit 1
            ;;
    esac

    if [ "$auto_quality" = true ]; then
        temp_dir=$(mktemp -d)  # Temporary directory for intermediate files
        for file in "$input_dir"/*; do
            if [ -f "$file" ]; then
                ext="${file##*.}"
                ext_lower=$(echo "$ext" | tr '[:upper:]' '[:lower:]')
                supported=false
                # Check if file extension is supported
                for supp in "${supported_exts[@]}"; do
                    if [ "$ext_lower" = "$supp" ]; then
                        supported=true
                        break
                    fi
                done
                if [ "$supported" = true ]; then
                    filename="${file##*/}"
                    filename="${filename%.*}"
                    # Find optimal quality and encode
                    optimal_quality=$(find_optimal_quality "$file" "$encode_format" "$temp_dir")
                    case "$encode_format" in
                        jxl )
                            options="-q $optimal_quality"
                            ;;
                        avif )
                            quantizer=$(( (100 - optimal_quality) * 63 / 100 ))
                            options="--min $quantizer --max $quantizer --jobs $(nproc)"
                            ;;
                    esac
                    output_file="$output_dir/$filename$output_ext"
                    [ "$verbose" = true ] && echo "Encoding: $file -> $output_file with options: $options"
                    if ! "$encoder" $options "$file" "$output_file"; then
                        echo "Warning: Failed to encode $file"
                    else
                        processed=true
                    fi
                else
                    [ "$verbose" = true ] && echo "Skipping unsupported file: $file"
                fi
            fi
        done
        rm -rf "$temp_dir"  # Clean up temporary directory
    else
        # Fixed-quality encoding
        options=""
        case "$encode_format" in
            jxl )
                options="-q $quality"
                [ "$lossless" = true ] && options="-d 0"  # Distance 0 for lossless
                ;;
            avif )
                if [ "$lossless" = true ]; then
                    options="--lossless -y 444 --cicp 1/13/0"
                else
                    quantizers=$(get_avif_quantizers "$quality")
                    options="$quantizers"
                fi
                options="$options --jobs $(nproc)"  # Use all CPU cores
                ;;
        esac
        for file in "$input_dir"/*; do
            if [ -f "$file" ]; then
                ext="${file##*.}"
                ext_lower=$(echo "$ext" | tr '[:upper:]' '[:lower:]')
                supported=false
                for supp in "${supported_exts[@]}"; do
                    if [ "$ext_lower" = "$supp" ]; then
                        supported=true
                        break
                    fi
                done
                if [ "$supported" = true ]; then
                    filename="${file##*/}"
                    filename="${filename%.*}"
                    output_file="$output_dir/$filename$output_ext"
                    [ "$verbose" = true ] && echo "Encoding: $file -> $output_file with options: $options"
                    if ! "$encoder" $options "$file" "$output_file"; then
                        echo "Warning: Failed to encode $file"
                    else
                        processed=true
                    fi
                else
                    [ "$verbose" = true ] && echo "Skipping unsupported file: $file"
                fi
            fi
        done
    fi
fi

# Decoding logic
if [ -n "$decode_format" ]; then
    # Set decoder and input extension based on format
    case "$decode_format" in
        jxl )
            decoder="djxl"
            input_ext=".jxl"
            ;;
        avif )
            decoder="avifdec"
            input_ext=".avif"
            ;;
        * )
            echo "Error: Unsupported decode format: $decode_format (use jxl or avif)"
            exit 1
            ;;
    esac

    # Default to PNG if no output format specified
    [ -z "$output_format" ] && output_format="png"
    case "$output_format" in
        png ) output_ext=".png" ;;
        jpg | jpeg ) output_ext=".jpg" ;;
        * )
            echo "Error: Unsupported output format: $output_format (use png or jpg)"
            exit 1
            ;;
    esac

    for file in "$input_dir"/*"$input_ext"; do
        if [ -f "$file" ]; then
            filename="${file##*/}"
            filename="${filename%$input_ext}"
            output_file="$output_dir/$filename$output_ext"
            [ "$verbose" = true ] && echo "Decoding: $file -> $output_file"
            if ! "$decoder" "$file" "$output_file"; then
                echo "Warning: Failed to decode $file"
            else
                processed=true
            fi
        fi
    done
fi

# Provide feedback on whether any files were processed
if [ "$processed" = false ]; then
    echo "No files processed. Check input directory and file extensions."
else
    echo "Operation complete."
fi