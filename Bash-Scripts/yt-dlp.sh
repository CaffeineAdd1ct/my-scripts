#!/bin/bash

# Check if the URL argument is provided
if [ $# -eq 0 ]; then
    echo "Please provide a YouTube channel URL or a YouTube video URL as an argument."
    exit 1
fi

# Specify the directory where the videos will be downloaded
download_dir="/path/to/download/directory"

# Set the highest quality format available
quality="bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best"

# Function to download video by URL
download_video() {
    local url=$1
    yt-dlp --output "$download_dir/%(title)s.%(ext)s" --format "$quality" "$url"
}

# Process each URL provided as an argument
for url in "$@"; do
    # Check if it's a YouTube channel URL
    if [[ $url == *"/channel/"* ]] || [[ $url == *"/user/"* ]] || [[ $url == *"/c/"* ]]; then
        # Extract the YouTube channel ID from the URL
        channel_id=$(echo "$url" | grep -oP '(?<=channel/|user/|c/|/channel/|/user/|/c/)[^/]+')

        # Check if the channel ID is extracted successfully
        if [ -z "$channel_id" ]; then
            echo "Invalid YouTube channel URL: $url. Skipping..."
        else
            # Fetch all video URLs from the YouTube channel
            video_urls=$(yt-dlp --flat-playlist -j "$url" | jq -r '.entries[].webpage_url')

            # Iterate over each video URL and download in highest quality
            for video_url in $video_urls; do
                download_video "$video_url"
            done
        fi
    else
        # Assume it's a single YouTube video URL
        download_video "$url"
    fi
done

