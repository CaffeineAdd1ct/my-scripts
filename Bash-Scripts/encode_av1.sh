#!/bin/bash

# Check if ab-av1 is installed
if ! command -v ab-av1 &> /dev/null; then
    echo "Error: ab-av1 is not installed or not in PATH."
    exit 1
fi

# Function to display help message
display_help() {
    echo "Usage: $0 <input_file_or_directory> [additional_flags...]"
    echo ""
    echo "Encode video(s) using ab-av1 with the av1_qsv encoder for Intel GPUs."
    echo ""
    echo "**Features:**"
    echo "- Encode a single video file."
    echo "- Batch encode all .mkv, .mp4, .webm, .avi files in a directory."
    echo "- Pass additional flags to ab-av1 auto-encode (e.g., --preset, --min-vmaf)."
    echo ""
    echo "**Examples:**"
    echo "    $0 input.mkv                     # Encode a single file"
    echo "    $0 input_folder/                 # Encode all supported files in folder"
    echo "    $0 input.mkv --preset 7          # Single file with preset"
    echo "    $0 input_folder/ --min-vmaf 95   # Batch encode with minimum VMAF"
    echo ""
    echo "**Additional Flags:**"
    echo "Any flags after the input are passed to ab-av1 auto-encode."
    echo "Common flags include:"
    echo "    --preset <number>    Set encoding preset (e.g., 7)"
    echo "    --min-vmaf <number>  Set minimum VMAF score (e.g., 95)"
    echo "For all supported flags, run: ab-av1 auto-encode --help"
}

# Check for help flag
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    display_help
    exit 0
fi

# Check if input is provided
if [ -z "$1" ]; then
    echo "Error: No input provided."
    display_help
    exit 1
fi

# Set input and shift arguments
input="$1"
shift

# Function to encode a single file
encode_file() {
    local input="$1"
    shift
    echo "Encoding: $input"
    ab-av1 auto-encode -i "$input" -e av1_qsv "$@"
}

# Function to encode all video files in a directory
encode_directory() {
    local dir="$1"
    shift
    local found_files=0
    for file in "$dir"/*.{mkv,mp4,webm,avi}; do
        if [ -f "$file" ]; then
            found_files=1
            encode_file "$file" "$@"
        fi
    done
    if [ $found_files -eq 0 ]; then
        echo "Warning: No .mkv, .mp4, .webm, or .avi files found in $dir"
    fi
}

# Process input based on whether it's a file or directory
if [ -f "$input" ]; then
    encode_file "$input" "$@"
elif [ -d "$input" ]; then
    encode_directory "$input" "$@"
else
    echo "Error: '$input' is not a valid file or directory."
    exit 1
fi