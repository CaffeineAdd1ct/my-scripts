#!/bin/bash -e

# Colors for output
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Emoji symbols
LOCK="🔒"
UNLOCK="🔓"

# Ensure dialog is installed
if ! command -v dialog >/dev/null 2>&1; then
    echo -e "${RED}Error: 'dialog' is required but not installed. Please install it (e.g., 'sudo apt install dialog' on Debian/Ubuntu).${NC}" >&2
    exit 1
fi

# Temporary file for dialog output
TEMP=$(mktemp)
trap 'rm -f $TEMP' EXIT

# Help message for CLI usage
help_message() {
    echo -e "${BLUE}Usage: ${0} [file]${NC}"
    echo -e "${BLUE}For TUI mode: run without arguments (${0})${NC}"
    echo -e "${BLUE}For CLI mode: provide a file to encrypt/decrypt${NC}"
    exit 1
}

# Check if rage is installed
if ! command -v rage >/dev/null 2>&1; then
    echo -e "${RED}Error: 'rage' is not installed. Please install it (see https://github.com/str4d/rage).${NC}" >&2
    exit 1
fi

# CLI mode (if file provided)
if [ $# -eq 1 ]; then
    if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
        help_message
    fi
    INPUT_FILE="$1"
    if [[ "$INPUT_FILE" == *.age ]]; then
        DECRYPT=true
        OUTPUT_NAME="${INPUT_FILE%.age}"
        echo -e "${UNLOCK} ${BLUE}Decrypting: $INPUT_FILE${NC}"
        if ! rage --decrypt --output "$OUTPUT_NAME" "$INPUT_FILE"; then
            echo -e "${UNLOCK} ${RED}Decryption failed${NC}" >&2
            exit 1
        fi
        echo -e "${UNLOCK} ${GREEN}File decrypted to ${OUTPUT_NAME}${NC}"
    else
        DECRYPT=false
        OUTPUT_NAME="${INPUT_FILE}.age"
        echo -e "${LOCK} ${BLUE}Encrypting: $INPUT_FILE${NC}"
        if ! rage --encrypt --passphrase --output "$OUTPUT_NAME" "$INPUT_FILE"; then
            echo -e "${LOCK} ${RED}Encryption failed${NC}" >&2
            exit 1
        fi
        echo -e "${LOCK} ${GREEN}File encrypted to ${OUTPUT_NAME}${NC}"
    fi
    read -p "Remove original file? [y/N] " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        rm "$INPUT_FILE" 2>/dev/null && echo -e "${GREEN}Original file removed${NC}"
    fi
    exit 0
fi

# TUI Functions

# File selection dialog
select_file() {
    dialog --title "Select File" --fselect "$HOME/" 14 48 2>"$TEMP"
    INPUT_FILE=$(cat "$TEMP")
    if [ -z "$INPUT_FILE" ] || [ ! -f "$INPUT_FILE" ]; then
        dialog --msgbox "No valid file selected. Exiting." 6 40
        exit 1
    fi
}

# Main menu
main_menu() {
    dialog --title "Rage Encryption Tool" --menu "Choose an operation:" 15 50 5 \
        1 "Encrypt a File" \
        2 "Decrypt a File" \
        3 "Generate Key Pair" \
        4 "Exit" 2>"$TEMP"
    CHOICE=$(cat "$TEMP")
    case $CHOICE in
        1) encrypt_menu ;;
        2) decrypt_menu ;;
        3) generate_key ;;
        4) exit 0 ;;
        *) exit 0 ;;
    esac
}

# Encryption menu
encrypt_menu() {
    select_file
    dialog --title "Encryption Options" --menu "How would you like to encrypt '$INPUT_FILE'?" 15 60 6 \
        1 "With Passphrase" \
        2 "With Recipient Public Key" \
        3 "With Recipients File" \
        4 "With Armor (PEM Encoding)" \
        5 "Custom (Advanced Options)" \
        6 "Back" 2>"$TEMP"
    ENC_CHOICE=$(cat "$TEMP")
    OUTPUT_NAME="${INPUT_FILE}.age"
    case $ENC_CHOICE in
        1) encrypt_passphrase ;;
        2) encrypt_recipient ;;
        3) encrypt_recipients_file ;;
        4) encrypt_armor ;;
        5) encrypt_custom ;;
        6) main_menu ;;
        *) main_menu ;;
    esac
}

# Decryption menu
decrypt_menu() {
    select_file
    if [[ "$INPUT_FILE" != *.age ]]; then
        dialog --msgbox "Selected file is not an .age file. Please select an encrypted file." 6 40
        decrypt_menu
        return
    fi
    OUTPUT_NAME="${INPUT_FILE%.age}"
    dialog --title "Decryption Options" --menu "Decrypt '$INPUT_FILE' using:" 12 50 3 \
        1 "Passphrase (Auto-Detect)" \
        2 "Identity File" \
        3 "Back" 2>"$TEMP"
    DEC_CHOICE=$(cat "$TEMP")
    case $DEC_CHOICE in
        1) decrypt_passphrase ;;
        2) decrypt_identity ;;
        3) main_menu ;;
        *) main_menu ;;
    esac
}

# Generate key pair
generate_key() {
    dialog --inputbox "Enter output file for key pair (e.g., key.txt):" 8 40 "$HOME/key.txt" 2>"$TEMP"
    KEY_FILE=$(cat "$TEMP")
    if [ -z "$KEY_FILE" ]; then
        dialog --msgbox "No file specified. Returning to menu." 6 40
        main_menu
        return
    fi
    if rage-keygen -o "$KEY_FILE" >"$TEMP" 2>&1; then
        PUBLIC_KEY=$(cat "$TEMP")
        dialog --msgbox "Key pair generated successfully!\nPublic Key: $PUBLIC_KEY\nSaved to: $KEY_FILE" 10 60
    else
        dialog --msgbox "Failed to generate key pair. Check permissions or path." 6 40
    fi
    main_menu
}

# Encryption with passphrase
encrypt_passphrase() {
    CMD="rage --encrypt --passphrase --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File encrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Encryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Encryption with recipient
encrypt_recipient() {
    dialog --inputbox "Enter recipient public key (e.g., age1... or ssh-...):" 8 60 2>"$TEMP"
    RECIPIENT=$(cat "$TEMP")
    if [ -z "$RECIPIENT" ]; then
        dialog --msgbox "No recipient provided. Returning to menu." 6 40
        encrypt_menu
        return
    fi
    CMD="rage --encrypt -r '$RECIPIENT' --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File encrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Encryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Encryption with recipients file
encrypt_recipients_file() {
    dialog --fselect "$HOME/" 14 48 2>"$TEMP"
    RECIPIENTS_FILE=$(cat "$TEMP")
    if [ -z "$RECIPIENTS_FILE" ] || [ ! -f "$RECIPIENTS_FILE" ]; then
        dialog --msgbox "No valid recipients file selected." 6 40
        encrypt_menu
        return
    fi
    CMD="rage --encrypt -R '$RECIPIENTS_FILE' --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File encrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Encryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Encryption with armor
encrypt_armor() {
    dialog --inputbox "Enter recipient public key (optional, leave blank for passphrase):" 8 60 2>"$TEMP"
    RECIPIENT=$(cat "$TEMP")
    if [ -z "$RECIPIENT" ]; then
        CMD="rage --encrypt --passphrase -a --output '$OUTPUT_NAME' '$INPUT_FILE'"
    else
        CMD="rage --encrypt -r '$RECIPIENT' -a --output '$OUTPUT_NAME' '$INPUT_FILE'"
    fi
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File encrypted with armor to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Encryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Custom encryption
encrypt_custom() {
    dialog --form "Custom Encryption Options" 15 60 8 \
        "Recipient (e.g., age1...):" 1 1 "" 1 25 30 0 \
        "Recipients File:" 2 1 "" 2 25 30 0 \
        "Use Armor (y/N):" 3 1 "N" 3 25 5 1 \
        "Output File:" 4 1 "$OUTPUT_NAME" 4 25 30 0 2>"$TEMP"
    IFS=$'\n' read -r RECIPIENT RECIPIENTS_FILE ARMOR CUSTOM_OUTPUT <"$TEMP"
    if [ -n "$CUSTOM_OUTPUT" ]; then
        OUTPUT_NAME="$CUSTOM_OUTPUT"
    fi
    CMD="rage --encrypt"
    [ -n "$RECIPIENT" ] && CMD="$CMD -r '$RECIPIENT'"
    [ -n "$RECIPIENTS_FILE" ] && [ -f "$RECIPIENTS_FILE" ] && CMD="$CMD -R '$RECIPIENTS_FILE'"
    [ "$ARMOR" = "y" ] || [ "$ARMOR" = "Y" ] && CMD="$CMD -a"
    CMD="$CMD --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File encrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Encryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Decryption with passphrase
decrypt_passphrase() {
    CMD="rage --decrypt --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File decrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Decryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Decryption with identity
decrypt_identity() {
    dialog --fselect "$HOME/" 14 48 2>"$TEMP"
    IDENTITY_FILE=$(cat "$TEMP")
    if [ -z "$IDENTITY_FILE" ] || [ ! -f "$IDENTITY_FILE" ]; then
        dialog --msgbox "No valid identity file selected." 6 40
        decrypt_menu
        return
    fi
    CMD="rage --decrypt -i '$IDENTITY_FILE' --output '$OUTPUT_NAME' '$INPUT_FILE'"
    if eval "$CMD" 2>"$TEMP"; then
        dialog --msgbox "File decrypted successfully to '$OUTPUT_NAME'" 6 40
        cleanup_prompt
    else
        dialog --msgbox "Decryption failed:\n$(cat "$TEMP")" 10 50
    fi
    main_menu
}

# Cleanup prompt
cleanup_prompt() {
    dialog --yesno "Remove original file '$INPUT_FILE'?" 6 40
    if [ $? -eq 0 ]; then
        rm "$INPUT_FILE" 2>/dev/null && dialog --msgbox "Original file removed successfully" 6 40
    fi
}

# Start the TUI
clear
main_menu