#!/bin/bash

# Clear package cache
sudo pacman -Scc --noconfirm

# Refresh the package keyring
sudo rm -r /etc/pacman.d/gnupg
sudo pacman-key --init
sudo pacman-key --populate archlinux

# Retry the installation
sudo pacman -Syu --noconfirm

exit 0
