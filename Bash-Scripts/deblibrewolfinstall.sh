#!/bin/bash

# Update package lists and install required dependencies
sudo apt update
sudo apt install -y wget gnupg lsb-release apt-transport-https ca-certificates

# Determine the distribution codename
distro=$(if echo " una bookworm vanessa focal jammy bullseye vera uma " | grep -q " $(lsb_release -sc) "; then echo $(lsb_release -sc); else echo focal; fi)

# Download and import the GPG key
wget -O- https://deb.librewolf.net/keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/librewolf.gpg

# Add LibreWolf repository to package sources
sudo tee /etc/apt/sources.list.d/librewolf.sources <<EOF > /dev/null
Types: deb
URIs: https://deb.librewolf.net
Suites: $distro
Components: main
Architectures: amd64
Signed-By: /usr/share/keyrings/librewolf.gpg
EOF

# Update package lists again
sudo apt update

# Install LibreWolf
sudo apt install librewolf -y

echo "LibreWolf installation completed!" 
