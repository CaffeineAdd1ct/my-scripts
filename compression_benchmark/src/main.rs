use std::process::Command;
use std::fs;
use std::io::{self, Write};
use std::path::Path;
use clap::{Arg, Command as ClapCommand};
use comfy_table::{Table, Cell};

struct Trial {
    name: String,
    command: String,
    extension: String,
}

struct Result {
    name: String,
    size: u64,
    memory: u64,
}

fn human_readable_size(bytes: u64) -> String {
    const UNITS: &[&str] = &["B", "KB", "MB", "GB"];
    let mut size = bytes as f64;
    let mut unit_idx = 0;
    while size >= 1024.0 && unit_idx < UNITS.len() - 1 {
        size /= 1024.0;
        unit_idx += 1;
    }
    format!("{:.2} {}", size, UNITS[unit_idx])
}

fn main() -> io::Result<()> {
    // Parse command-line arguments
    let matches = ClapCommand::new("Compression Benchmark")
        .arg(
            Arg::new("INPUT")
                .help("Input file or folder")
                .required(true)
                .index(1)
        )
        .get_matches();
    let input_path: &str = matches.get_one::<String>("INPUT").expect("INPUT is required");

    // Validate input
    if !Path::new(input_path).exists() {
        eprintln!("Error: Input '{}' does not exist.", input_path);
        std::process::exit(1);
    }

    // Create output directory
    let input_name = Path::new(input_path).file_name().unwrap().to_str().unwrap();
    let output_dir = format!("{}_compression_test", input_name);
    fs::create_dir_all(&output_dir)?;

    // Define compression trials
    let trials = vec![
        Trial { name: "gzip-1".to_string(), command: "gzip -1".to_string(), extension: "gz".to_string() },
        Trial { name: "gzip-6".to_string(), command: "gzip -6".to_string(), extension: "gz".to_string() },
        Trial { name: "gzip-9".to_string(), command: "gzip -9".to_string(), extension: "gz".to_string() },
        Trial { name: "bzip2-1".to_string(), command: "bzip2 -1".to_string(), extension: "bz2".to_string() },
        Trial { name: "bzip2-6".to_string(), command: "bzip2 -6".to_string(), extension: "bz2".to_string() },
        Trial { name: "bzip2-9".to_string(), command: "bzip2 -9".to_string(), extension: "bz2".to_string() },
        Trial { name: "xz-0".to_string(), command: "xz -0".to_string(), extension: "xz".to_string() },
        Trial { name: "xz-6".to_string(), command: "xz -6".to_string(), extension: "xz".to_string() },
        Trial { name: "xz-9".to_string(), command: "xz -9".to_string(), extension: "xz".to_string() },
        Trial { name: "xz-9_extreme".to_string(), command: "xz -9 --extreme".to_string(), extension: "xz".to_string() },
        Trial { name: "zstd-1".to_string(), command: "zstd -1".to_string(), extension: "zst".to_string() },
        Trial { name: "zstd-10".to_string(), command: "zstd -10".to_string(), extension: "zst".to_string() },
        Trial { name: "zstd-19".to_string(), command: "zstd -19".to_string(), extension: "zst".to_string() },
        Trial { name: "zstd-22".to_string(), command: "zstd -22".to_string(), extension: "zst".to_string() },
        Trial { name: "brotli-0".to_string(), command: "brotli -0".to_string(), extension: "br".to_string() },
        Trial { name: "brotli-5".to_string(), command: "brotli -5".to_string(), extension: "br".to_string() },
        Trial { name: "brotli-11".to_string(), command: "brotli -11".to_string(), extension: "br".to_string() },
        Trial { name: "lz4-1".to_string(), command: "lz4 -1".to_string(), extension: "lz4".to_string() },
        Trial { name: "lz4-9".to_string(), command: "lz4 -9".to_string(), extension: "lz4".to_string() },
        Trial { name: "lz4-12".to_string(), command: "lz4 -12".to_string(), extension: "lz4".to_string() },
        Trial { name: "lzo-1".to_string(), command: "lzop -1".to_string(), extension: "lzo".to_string() },
        Trial { name: "lzo-5".to_string(), command: "lzop -5".to_string(), extension: "lzo".to_string() },
        Trial { name: "lzo-9".to_string(), command: "lzop -9".to_string(), extension: "lzo".to_string() },
    ];

    // Run compression trials
    let mut results = Vec::new();
    for trial in &trials {
        let output_file = format!("{}/{}.tar.{}", output_dir, trial.name, trial.extension);
        println!("Compressing with {}...", trial.name);

        let output = Command::new("/usr/bin/time")
            .arg("-v")
            .arg("tar")
            .arg("--use-compress-program")
            .arg(&trial.command)
            .arg("-cf")
            .arg(&output_file)
            .arg(input_path)
            .output();

        match output {
            Ok(output) if output.status.success() => {
                let size = fs::metadata(&output_file)?.len();
                let stderr = String::from_utf8_lossy(&output.stderr);
                let memory = stderr.lines()
                    .find(|line| line.contains("Maximum resident set size"))
                    .and_then(|line| line.split(':').nth(1))
                    .and_then(|val| val.trim().parse::<u64>().ok())
                    .unwrap_or(0);

                results.push(Result {
                    name: trial.name.clone(),
                    size,
                    memory,
                });
            }
            _ => {
                println!("Warning: Compression with {} failed.", trial.name);
            }
        }
    }

    // Build and display table
    let mut table = Table::new();
    table.set_header(vec!["Compressor", "Size", "Memory Used"]);
    for result in &results {
        table.add_row(vec![
            Cell::new(&result.name),
            Cell::new(&human_readable_size(result.size)),
            Cell::new(&human_readable_size(result.memory * 1024)), // Convert kB to bytes
        ]);
    }
    println!("{}", table);

    // Prompt user to keep files
    print!("Keep all files or only the best? (all/best): ");
    io::stdout().flush()?;
    let mut choice = String::new();
    io::stdin().read_line(&mut choice)?;
    let choice = choice.trim().to_lowercase();

    if choice == "best" {
        let min_size = results.iter().map(|r| r.size).min().unwrap_or(0);
        for result in &results {
            if result.size > min_size {
                let file = format!("{}/{}.tar.{}", output_dir, result.name, trials.iter().find(|t| t.name == result.name).unwrap().extension);
                fs::remove_file(&file)?;
            }
        }
        println!("Kept only the smallest file(s).");
    } else {
        println!("Kept all files.");
    }

    Ok(())
}