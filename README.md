# Linux Scripts Repository

Welcome to the Linux Scripts repository! This repository is a collection of various scripts that can be used on Linux systems, including Arch Linux. The scripts provided here aim to automate tasks, provide useful functionalities, and offer solutions to common Linux-related scenarios.

## Getting Started

To get started with the scripts in this repository, follow the steps below:

1. Clone the repository to your local machine:

   ```bash
   git clone https://gitlab.com/CaffeineAdd1ct/my-scripts.git && cd my-scripts
   ```

Here you are able to use the scripts in the terminal by typing `./script.sh` followed by the flags for it.

## Scripts

<details>
<summary><h5>encode_av1.sh</h5></summary>

## Overview

`encode_av1.sh` is a bash script that streamlines video encoding to the AV1 format using the `ab-av1` tool, leveraging Intel GPU acceleration with the `av1_qsv` encoder. It supports encoding both individual files and batches of files within a directory, while allowing users to customize the process with additional `ab-av1` flags.

### Key Features
- **Single File or Batch Encoding**: Process a single video or all supported videos in a folder.
- **Intel GPU Acceleration**: Utilizes the `av1_qsv` encoder for fast, hardware-accelerated encoding.
- **Customizable Flags**: Add `ab-av1` options like `--preset` or `--min-vmaf` for tailored results.
- **Skip Existing Files**: Avoids re-encoding if the output file already exists.
- **Help Option**: Displays a detailed help message with usage and examples.

### Supported Input Formats
- `.mkv`
- `.mp4`
- `.webm`

### Output Format
- Outputs are saved as `.mkv` files with an `_av1` suffix (e.g., `input.mkv` becomes `input_av1.mkv`).

---

## Usage

### Basic Usage
- **Single File**:  
  ```bash
  ./encode_av1.sh input.mkv
  ```
- **Directory (Batch Encoding)**:  
  ```bash
  ./encode_av1.sh input_folder/
  ```

### With Additional Flags
Pass any `ab-av1 auto-encode` flags after the input:
- **Set a Preset**:  
  ```bash
  ./encode_av1.sh input.mkv --preset 7
  ```
- **Set a Minimum VMAF**:  
  ```bash
  ./encode_av1.sh input_folder/ --min-vmaf 95
  ```

### Help Option
View detailed usage instructions:  
```bash
./encode_av1.sh -h
```  
or  
```bash
./encode_av1.sh --help
```

---

## Features in Detail

### 1. Single File Encoding
Encodes a single video file with:  
```bash
ab-av1 auto-encode -i input.mkv -e av1_qsv -o input_av1.mkv
```

### 2. Batch Encoding
Processes all `.mkv`, `.mp4`, and `.webm` files in a directory, appending `_av1.mkv` to each output filename.

### 3. Additional Flags
Flags provided after the input are passed to `ab-av1`. For example:  
```bash
./encode_av1.sh input.mkv --preset 7 --min-vmaf 95
```  
Executes:  
```bash
ab-av1 auto-encode -i input.mkv -e av1_qsv -o input_av1.mkv --preset 7 --min-vmaf 95
```

### 4. Skip Existing Files
Skips encoding if the output (e.g., `input_av1.mkv`) already exists, saving time.

### 5. Help Message
The `-h` or `--help` flag shows:
- Usage syntax
- Script description
- Common `ab-av1` flags
- Example commands

---

## Customization

### Modify Supported Formats
Edit this line to change input formats:  
```bash
for file in "$input"/*.{mkv,mp4,webm}; do
```  
To add `.avi`, use:  
```bash
for file in "$input"/*.{mkv,mp4,webm,avi}; do
```

### Force Overwrite
To overwrite existing files, replace:  
```bash
if [ -f "$output" ]; then
    echo "Output file $output already exists. Skipping."
else
    ab-av1 auto-encode -i "$file" -e av1_qsv -o "$output" "$@"
fi
```  
With:  
```bash
ab-av1 auto-encode -i "$file" -e av1_qsv -o "$output" "$@"
```

### Add Default Flags
Set default flags by editing the `ab-av1` command. For `--preset 4` on all encodes:  
Change:  
```bash
ab-av1 auto-encode -i "$file" -e av1_qsv -o "$output" "$@"
```  
To:  
```bash
ab-av1 auto-encode -i "$file" -e av1_qsv --preset 4 -o "$output" "$@"
```

---

## Examples

### Encode a Single File
```bash
./encode_av1.sh video.mkv
```  
- Output: `video_av1.mkv`

### Batch Encode with a Preset
```bash
./encode_av1.sh videos/ --preset 7
```  
- Encodes all supported files in `videos/` using preset 7.

### Encode with Minimum VMAF
```bash
./encode_av1.sh input.mp4 --min-vmaf 95
```  
- Ensures a VMAF score of at least 95.

### Use a Different Encoder
```bash
./encode_av1.sh input.mkv -e svt-av1
```  
- Overrides `av1_qsv` with `svt-av1`.

---

## Notes
- Ensure `ab-av1` is installed and in your `PATH`.
- Outputs use the `.mkv` container; modify the script’s output extension for other formats.
- For advanced `ab-av1` options, see the [ab-av1 documentation](https://github.com/alexheretic/ab-av1).
</details>

---

<details>
<summary><h5>imgconvert.sh</h5></summary>

# Image Conversion Script (`imgconvert.sh`)

`imgconvert.sh` is a versatile Bash script designed for encoding and decoding images in modern formats like JPEG XL (JXL) and AVIF. It offers flexible options, including fixed-quality encoding, lossless compression, and an automated quality control feature that optimizes compression based on the Structural Similarity Index (SSIM).

## Features

- **Encoding**: Convert PNG, JPG, or JPEG images to JXL or AVIF with customizable quality settings.
- **Decoding**: Convert JXL or AVIF images back to PNG or JPG.
- **Automated Quality Control**: Use the `-a` option to find the lowest quality setting that meets a specified SSIM threshold (default: 0.99).
- **Lossless Encoding**: Enable lossless compression with the `-l` flag.
- **Verbose Output**: Use `-v` to see detailed processing information.
- **Multi-threaded AVIF Encoding**: Utilizes all CPU cores for faster processing.

## Requirements

### Tools

- `cjxl` and `djxl` (from `libjxl-tools`) for JXL encoding/decoding.
- `avifenc` and `avifdec` (from `libavif`) for AVIF encoding/decoding.
- `ffmpeg-quality-metrics` and `jq` for SSIM-based automated quality control.
- `bc` for floating-point arithmetic in SSIM comparisons.

### Supported Formats

- **Encoding Input**: PNG, JPG, JPEG.
- **Decoding Input**: JXL, AVIF.
- **Decoding Output**: PNG, JPG.

## Installation

1. **Make the Script Executable**:
    ```bash
    chmod +x imgconvert.sh
    ```

2. **Install Dependencies**(e.g., on Ubuntu):
    ```bash
    sudo apt update
    sudo apt install libjxl-tools libavif-bin python3 python3-pip jq pipx
    ```
    Install with python:
    ```
    pipx install ffmpeg-quality-metrics
    pipx ensurepath
    ```
    Verify installations:
    ```bash
    cjxl --version
    avifenc --version
    jq --version
    bc --version
    ```

## Usage

    ```bash
    ./imgconvert.sh [OPTIONS] input_directory output_directory
    ```
### Options

- `-e, --encode FORMAT`: Encode images to jxl or avif.
- `-d, --decode FORMAT`: Decode images from jxl or avif.
- `-o, --output OUTPUT_FORMAT`: Set decoding output format (png or jpg), default: png.
- `-q, --quality QUALITY`: Set encoding quality (0-100), default: 80.
- `-l, --lossless`: Enable lossless encoding (only with -e).
- `-a, --auto-quality`: Enable SSIM-based automated quality control (only with -e).
- `--ssim-threshold VALUE`: Set SSIM threshold for auto-quality (0.0-1.0), default: 0.99.
- `-v, --verbose`: Display detailed processing information.
- `-h, --help`: Show help message.

### Examples

1. **Encode PNG/JPG to AVIF with Quality 85**:
   ```bash
   ./imgconvert.sh -e avif -q 85 images/ output/
   ```

2. **Decode JXL to JPG**:
   ```bash
   ./imgconvert.sh -d jxl -o jpg images/ output/
   ```

3. **Encode to AVIF with Automated Quality**:
   ```bash
   ./imgconvert.sh -e avif -a images/ output/
   ```

4. **Encode to JXL with Custom SSIM Threshold**:
   ```bash
   ./imgconvert.sh -e jxl -a --ssim-threshold 0.95 images/ output/
   ```

5. **Lossless Encoding to JXL**:
   ```bash
   ./imgconvert.sh -e jxl -l images/ output/
   ```

## Notes

- **Automated Quality Control**: When `-a` is used, the script performs a binary search to determine the lowest quality that meets the SSIM threshold. This process is slower but optimizes file size while maintaining quality.
- **File Overwriting**: Existing files in the output directory will be overwritten without warning.
- **Temporary Files**: Auto-quality mode creates temporary files in `/tmp`, which are removed after processing.
- **Error Handling**: The script validates inputs, checks for required tools, and skips unsupported files. Use `-v` for detailed diagnostics.

## Troubleshooting

- **"Tool not found"**: Ensure all required tools are installed and accessible in your PATH.
- **"No files processed"**: Verify that the input directory contains files with supported extensions.
- **Permission Issues**: Check read permissions for `input_directory` and write permissions for `output_directory`.
</details>

---

<details>
<summary><h5>enable_user_mount.sh</h5></summary>

## User Mount Permissions Script

This script enables a user to mount a device without requiring root privileges. It modifies the PolicyKit configuration to grant the user permission for mounting. By running this script, you can enable user mount permissions on your system. Here's an overview of what the script does:

1. **Modifying PolicyKit Configuration**: The script modifies the PolicyKit configuration to grant the specified user permission for mounting devices. It uses conditional logic to check if the user executing the script matches the specified username provided in the script. If the user matches, it proceeds to modify the PolicyKit configuration. The exact modifications made to the configuration may vary based on the system and PolicyKit version.

2. **Granting User Mount Permissions**: The script ensures that the specified user is granted the necessary permissions to mount devices. This eliminates the need for the user to enter root (sudo) password every time they want to mount a device. By modifying the PolicyKit configuration, the user gains the ability to mount devices as a regular user.

3. **Usage Instructions**: The script documentation should provide detailed instructions on how to use the script effectively. It typically requires specifying the username in the script before execution to ensure proper functionality. It's essential to follow the instructions and execute the script with the necessary permissions.

Please note that the script's exact implementation and modifications made to the PolicyKit configuration may vary based on the system, PolicyKit version, and specific requirements. It's important to review and customize the script to match your system's configuration and user specifications.

Before executing the script, ensure that you carefully follow the instructions provided in the script's documentation, specify your username correctly, and understand the implications of modifying the PolicyKit configuration.
</details>

---

<details>
<summary><h5>picshare.sh</h5></summary>

## Image Compression and EXIF Data Removal Script

This script automates the process of compressing images and removing EXIF data to ensure that no data can be traced back to the image's creator. It utilizes a few programs built into Linux or that need to be downloaded, namely FFmpeg and Exiftool. Here's an overview of what the script does:

1. **Image Compression**: The script compresses the images added to it. Compression reduces the file size of the images without significant loss in image quality. The specific method and level of compression applied may vary based on the tools used.

2. **EXIF Data Removal**: The script removes all EXIF data attached to each image. EXIF data includes information such as the camera make and model, date and time the photo was taken, GPS coordinates, and other metadata. Removing EXIF data helps protect the privacy of the image's creator by eliminating any potential traces or identifiable information.

3. **Required Programs**: The script relies on two programs, FFmpeg and Exiftool. FFmpeg is a versatile multimedia framework that can handle various tasks, including image compression. Exiftool is a command-line tool for reading, writing, and manipulating EXIF metadata. The script may check if these programs are already installed or download them if necessary.

Feel free to modify the script based on your requirements or preferences, ensuring that you have the necessary dependencies installed and use appropriate commands for image compression and EXIF data removal.
</details>

---

<details>
<summary><h5>setupvm.sh</h5></summary>

## Virtual Machine Setup Script

This script is used to set up a Virtual Machine (VM) using QEMU, KVM, Virt-manager, and their dependencies. It performs the following tasks:

1. **Installing the Program and Dependencies**: The script installs the necessary programs and their dependencies required for setting up and managing virtual machines. The specific package manager and commands used to install the programs may vary based on the distribution being used.

2. **Enabling and Starting the libvirtd Service**: The script enables and starts the `libvirtd` service to ensure it runs automatically during system boot. This service is responsible for managing virtualization capabilities and provides APIs for interacting with QEMU, KVM, and Virt-manager.

3. **Granting Necessary Permissions**: The script adds the user running the script to the `libvirt` group. By adding the user to this group, they gain necessary permissions and access rights to manage virtual machines using tools like Virt-manager. The specific command used to add the user to the group may vary based on the distribution being used.

These tasks collectively ensure that the necessary programs, services, and permissions are set up for creating and managing virtual machines using QEMU, KVM, and Virt-manager.

Please note that the specific package names, commands, and user/group management may vary depending on the distribution being used. It's important to adapt the script to match the specific package manager and user/group management commands for your distribution.
</details>

---

<details>
<summary><h5>fix_package_installation.sh</h5></summary>

## Package Cache Clearing and Keyring Refresh Script

This script performs the following tasks to clear the package cache, refresh the package keyring, and retry the installation:

1. **Clearing the Package Cache**: The script clears the package cache to remove any corrupted or invalid packages. This helps ensure a clean installation. The specific command used to clear the cache depends on the package manager being used.

2. **Refreshing the Package Keyring**: The script removes the existing package keyring and initializes a new one. This is done to ensure that the keyring is up to date and contains the latest keys from the Arch Linux keyring. The specific command used to refresh the keyring depends on the package manager being used.

3. **Retrying the Installation**: After clearing the cache and refreshing the keyring, the script synchronizes the package databases and upgrades all packages on the system. This step helps resolve conflicts or errors encountered during the previous installation attempt. The specific command used to retry the installation depends on the package manager being used.

These tasks collectively help address potential issues related to the package cache, keyring, and package conflicts, ensuring a smoother installation process.

Please note that the exact commands used may vary depending on the package manager or distribution being used. It's important to adapt the script to match the specific package management system in use.
</details>

---

<details>
<summary><h5>chaotic-aur-setup.sh</h5></summary>

## Chaotic-AUR Setup Script

This bash script automates the setup process for Chaotic-AUR, an automated building repository for AUR packages. Here's a breakdown of what it does:

1. **Installing Primary Key**: The script installs the primary key used for Chaotic-AUR using `sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com` and `sudo pacman-key --lsign-key 3056513887B78AEB`.

2. **Installing Chaotic-AUR Keyring and Mirrorlist**: The script installs the Chaotic-AUR keyring and mirrorlist packages from the official mirror using `sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'`.

3. **Adding Chaotic-AUR Repository Configuration**: The script appends a repository configuration to the `/etc/pacman.conf` file, enabling the use of the Chaotic-AUR repository. It adds the following lines to the file:

[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist

4. **Updating Package Lists**: The script updates the package lists to include packages from the Chaotic-AUR repository using `sudo pacman -Sy --noconfirm`.

5. **Completion Message**: Finally, the script displays a message indicating that the Chaotic-AUR setup is completed using `echo "Chaotic-AUR setup completed!"`.

</details>

---

<details>
<summary><h5>setuparch.sh</h5></summary>

## Arch Linux Setup Script

This script automates the setup process for Arch Linux. Here's a breakdown of what it does:

1. **Syncing Package Repositories**: The script starts by syncing the package repositories and updating the system using `sudo pacman -Syu --noconfirm`.

2. **Installing Reflector**: The script installs the `reflector` package, which is used to update the mirrorlist file.

3. **Generating New Mirrorlist**: `reflector` is used to generate a new mirrorlist file by selecting the fastest servers in the United States. The script uses `sudo reflector --verbose --country 'United States' --latest 50 --sort rate --save /etc/pacman.d/mirrorlist` to achieve this.

4. **Updating Mirrorlist**: After generating the new mirrorlist, the script updates the mirrorlist using `sudo pacman -Syy --noconfirm`.

5. **Installing Prerequisites**: The script installs `go` as a prerequisite for some of the subsequent steps using `sudo pacman -S --needed go --noconfirm`.

6. **Creating a New User**: A new user named "arch" is created with a randomly generated password. The script uses `sudo useradd -m -G wheel -s /bin/bash arch` to create the user and `echo -e "$random_password\n$random_password" | sudo passwd arch --quiet` to set the password.

7. **Allowing Passwordless Sudo**: The script allows passwordless sudo access for the newly created user by adding an entry to the `sudoers` file using `echo 'arch ALL=(ALL) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/arch`.

8. **Switching to the "arch" User**: The script switches to the "arch" user using `su -l arch` to execute the remaining steps as that user.

9. **Updating the System**: After switching to the "arch" user, the script updates the system once again using `sudo pacman -Syu --noconfirm`.

10. **Installing Fish Shell**: The script installs the Fish shell using `sudo pacman -S fish --noconfirm`.

11. **Installing Yay**: Yay, an AUR helper, is cloned from the AUR repository and installed using `makepkg` and `yay -S --noconfirm pacseek`.

12. **Changing Default Shell**: The default shell for the "arch" user is changed to Fish using `chsh -s /usr/bin/fish`.

13. **Installing pfetch**: The script clones the pfetch repository and installs it using `sudo make install`.

14. **Customizing Fish Shell**: Fish greeting and pfetch are configured in the Fish configuration file `~/.config/fish/config.fish` using `echo "set -U fish_greeting '🐟'"` and `echo "pfetch"`.

15. **Additional Setup for Chaotic-AUR**: The script sets up the Chaotic-AUR repository by adding the necessary keys, keyring, mirrorlist, and repository configuration to `/etc/pacman.conf`.

16. **Cleanup and Completion**: The script cleans up by removing passwordless sudo access, displaying the randomly generated password for the "arch" user, and starting the Fish shell.

</details>

---

<details>
<summary><h5>deblibrewolfinstall.sh</h5></summary>

## LibreWolf Installation Script

This script performs the following actions to install LibreWolf, a web browser based on Mozilla Firefox:

1. **Updating Package Lists and Installing Dependencies**: The script updates the package lists using the `apt` package manager and installs any required dependencies for the LibreWolf installation.

2. **Determining the Distribution Codename**: The script uses the `lsb_release -sc` command to determine the distribution codename. It assigns the codename to the `distro` variable. If the distribution codename is not found in the given list, "focal" is used as the default. The distribution codename is used to configure the package sources.

3. **Importing the GPG Key**: The script downloads the GPG key from the URL `https://deb.librewolf.net/keyring.gpg` and imports it using `gpg` with the `--dearmor` option. The dearmored key is then saved as `/usr/share/keyrings/librewolf.gpg`. Importing the key ensures the authenticity and integrity of the packages from the LibreWolf repository.

4. **Adding the LibreWolf Repository**: The script adds the LibreWolf repository to the package sources by creating a file named `librewolf.sources` in the `/etc/apt/sources.list.d/` directory. The file contains repository information such as URIs, suites, components, architectures, and the path to the imported GPG key. This step allows the package manager to retrieve packages from the LibreWolf repository.

5. **Updating Package Lists**: After adding the LibreWolf repository, the script updates the package lists again to include the newly added repository. This ensures that the package manager can find the LibreWolf package.

6. **Installing LibreWolf**: Finally, the script installs the LibreWolf package using the `apt` package manager. The package manager retrieves the package from the LibreWolf repository and installs it on the system.

Please note that this script assumes the usage of the `apt` package manager and is specifically designed for Debian-based distributions. It may require modification for other package managers or distributions. It's important to review and customize the script according to your specific requirements, package manager, and distribution.
</details>

---

<details>
<summary><h5>yt-dlp.sh</h5></summary>

## YouTube Video Downloader Script

This script is used to download YouTube videos using the `yt-dlp` command-line tool. It provides the following functionality:

1. **Downloading YouTube Videos**: The script accepts one or more YouTube channel URLs or YouTube video URLs as inputs. You can provide either a YouTube channel URL to download all videos from that channel or a YouTube video URL to download a specific video. The script uses the `yt-dlp` tool to perform the actual video downloading.

2. **Specifying Download Directory**: Before running the script, you need to specify the directory where the videos will be saved. Replace "/path/to/download/directory" with the actual path where you want to save the downloaded videos. The downloaded videos will be saved in this directory with their original titles.

3. **Downloading Highest Available Quality**: The script downloads the videos in the highest available quality. The exact quality of the downloaded videos depends on the options and preferences set in the `yt-dlp` tool.

4. **Requirements**: The script assumes that you have `yt-dlp` and `jq` installed on your system. `yt-dlp` is a command-line tool that provides additional features and flexibility compared to the standard `youtube-dl` tool. `jq` is a lightweight and flexible command-line JSON processor used for parsing and manipulating JSON data.

It's important to note that you need to have the `yt-dlp` and `jq` tools installed and accessible in your system's PATH before running the script. Additionally, make sure to replace "/path/to/download/directory" with the actual path where you want to save the downloaded videos.

Please review and customize the script according to your specific requirements, including the download directory and any additional options you may want to set in the `yt-dlp` tool.
</details>

---

<details>
<summary><h5>ragenc.sh</h5></summary>

## Encryption Script

This script is designed to encrypt files using the `rage` program, which is a fork of `age`. It provides the following functionality:

1. **Requirements**: The script assumes that you have the `rage` program installed on your system. Ensure that `rage` is properly installed before running the script.

2. **Encrypting Files**: The script accepts one input file and generates an encrypted version of that file using the `rage` program.

3. **Handling File Extensions**: The script automatically detects whether the provided file should be encrypted or decrypted based on its extension. If the file ends with `.age`, it is assumed to be a encrypted file and will be decrypted. Otherwise, it is assumed to be an unencrypted file and will be encrypted.

4. **Setting Output Name**: Before performing any operations, the script sets the output name for the encrypted or decrypted file. If decrypting, the `.age` extension is removed from the input file name. If encrypting, a `.age` extension is added to the input file name.

5. **Performing Encryption/Decryption**: The script uses the `rage` program to perform encryption or decryption based on the mode determined by the file extension. It provides feedback on the status of the operation and handles any errors that occur during execution.

6. **Optional Cleanup**: After completing the encryption or decryption, the script offers an option to remove the original input file. The user is prompted to confirm whether they want to remove the original file.

7. **Error Handling**: The script includes basic error handling mechanisms to manage common issues that may arise during encryption or decryption. It logs errors and provides informative messages to help troubleshoot problems.

8. **Help Message**: The script includes a help message to provide usage instructions for users who need assistance. This message is displayed if the user requests it via the `--help` or `-h` option.

9.  **Customization Options**: While the script does not offer extensive customization options, it can be modified to include additional features or parameters as needed by the user.

10. **Documentation**: Comprehensive documentation is provided in the form of comments within the script itself. This includes detailed explanations of each function, parameter, and option. Refer to this documentation for a deeper understanding of how the script operates.

It's important to note that you need to have the `rage` program installed on your system before running the script. Review and customize the script according to your specific requirements, including any additional options or error handling preferences.
</details>

---

<details>
<summary><h5>compression_benchmark</h5></summary>

## Compression Benchmarking Rust Program

The compression_benchmark is a Rust program that benchmarks the compression performance of various compression algorithms. It measures the time and compression ratio of files and folders using various compressors, providing users with an easy way to compare their performance.

**Installation**

**Required Tools:**

- `tar`
- `/usr/bin/time` (common on Linux)
- **Compressors:**
    - `gzip`
    - `bzip2`
    - `xz-utils`
    - `zstd`
    - `brotli`
    - `lz4`
    - `lzop`

**Installation Steps:**

1. Clone the repository: `git clone https://github.com/your-username/compression_benchmark.git`
2. Navigate to the directory: `cd compression_benchmark`
3. Install the required tools:
    - Run `sudo apt install gzip bzip2 xz-utils zstd brotli lz4 lzop` (on Debian/Ubuntu) or equivalent for your distro.
4. Compile the program:
    - Run `cargo build --release`

**Usage**

**Example:**

```
./target/release/compression_benchmark myfolder
```

**Options:**

- Replace `myfolder` with your input file or folder path.
- Follow the prompt to choose whether to keep all files or the best one.

**Output:**

The program will generate a table displaying the following information:

- Compressor name
- Input file size
- Compressed file size
- Compression ratio
- Compression time

**Customization**

While the program provides default options, it can be customized with additional features or parameters as needed. Refer to the `main.rs` file for more details.
</details>

## **All Hail Zabuton!!!**

```html
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢁⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣽⣿⡿⠉⡄⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⢷⣿⣿⠃⡱⠄⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⡿⣾⣿⡟⣠⣄⠃⣿⣿⣿⣿⣿⠿⠟⡛⢋⢩⢉⠡⣉⠩⣉⠙⡛⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣯⣷⣿⣿⢃⣿⣯⢇⣿⠟⢋⢡⠰⣈⠦⡑⣊⠔⣊⠱⢄⠣⡌⠱⢌⡱⢡⢂⠍⡛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣻⣿⣿⢸⣷⣻⢈⠠⡘⠤⢃⢎⡐⢆⡱⢐⠪⢄⡋⢤⠓⡌⡱⢢⠘⡔⡊⠜⡰⢡⢊⡙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⡏⠚⢹⠛⠀⡲⢉⡜⢌⠢⠜⣐⠢⢍⡒⢡⠜⠂⠵⠈⢂⠁⡉⠐⡁⠋⠴⢡⠢⡑⠆⡌⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⠟⣅⠰⢂⠬⣑⠀⢧⣌⣄⣣⣩⣤⣃⡦⢈⠡⡰⢘⠢⢍⢂⣓⠌⡑⣈⠱⢂⠤⢁⠬⣶⢿⣷⡜⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⢣⣾⡿⣆⡉⠒⡌⡱⢊⠹⣿⣻⠽⠛⣉⡐⢢⢡⢑⡊⡑⢊⠤⣁⠞⣠⢁⠏⡌⠲⢡⠒⡌⡙⠾⢙⡘⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⠃⡌⢄⡘⢉⡉⢂⡐⢅⠪⠔⡄⠄⣀⠇⠴⡈⠦⠡⠆⣴⣶⣶⡐⢢⢑⠢⡑⣠⣶⣶⡅⢣⠘⠰⢂⡄⠒⠜⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⡇⠒⠈⡤⠈⢡⢐⠢⠄⣂⢁⠚⠄⡸⠄⢪⠑⢰⣷⠑⡀⣿⣿⣿⡇⢂⠎⡱⡀⢿⣿⣿⡗⡨⠁⣿⠀⠄⠆⠱⢠⠙⣿⣿⣿⣿
⣿⣿⣿⣿⡇⠁⡓⠐⢈⠰⡈⡑⠆⠠⠋⡌⢂⠡⠚⣄⠪⢌⠩⡰⢁⡘⠻⠛⣀⠣⢊⠱⣐⠘⠿⠟⠡⢅⠣⢄⡍⠜⢀⠒⠥⠀⣿⣿⣿⣿
⣿⣿⣿⣿⡇⢠⣦⡗⣸⣷⡗⠀⢀⣷⣷⠀⠈⢀⢃⠢⠱⡌⠣⠔⠣⢌⡑⢢⠱⣈⠣⣑⠢⣑⠢⣉⠎⡢⡑⠎⡐⡁⢆⡘⢶⣦⣿⣿⣿⣿
⣿⣿⣿⣿⡇⣿⣽⢂⣿⣳⠃⠀⢨⡿⣾⠀⠈⠒⡌⠲⣰⣦⣍⡈⠓⠌⠌⣡⠒⣡⢒⡁⠎⣄⠣⢔⠡⢀⣥⣾⣇⠌⠆⠄⣾⣳⢽⣿⣿⣿
⣿⣿⣿⣿⠡⠁⠟⢀⠘⡙⠀⠀⠈⠟⠽⠀⠀⠈⠀⠳⣿⢯⣟⡿⠁⠤⣀⠂⠍⠐⠂⠀⠈⠀⠃⢈⢠⡉⣿⣳⡿⠂⠁⠈⠳⠻⢸⣿⣿⣿
⣿⣿⣿⠏⡐⡩⢀⢃⠖⠀⠀⠀⡜⢠⠆⠀⠀⠀⠀⠀⠀⠉⠙⠺⠐⠢⠅⠊⠀⠀⠀⠀⠀⢀⠀⠎⠔⠄⣜⡉⠀⠀⠀⠀⠰⡁⠆⣿⣿⣿
⡉⠤⠒⢠⠱⠀⠎⡌⠂⠖⡊⡐⢎⡡⠠⠄⠂⣀⣀⠠⠀⠀⠀⠀⠀⠤⢀⢀⡠⣀⠰⣊⠠⢀⠂⢄⡠⠴⣒⡒⠉⠒⠣⠤⡐⢉⡒⢈⠍⠛
⣤⣁⣆⢂⡉⠘⡘⠐⠊⠐⢀⠱⠂⢂⠰⠤⢒⣠⡤⠦⠑⡈⢄⡒⠭⠅⠂⣀⡐⠄⡂⠭⠂⠁⠈⠐⡠⠔⡀⡠⠶⠄⣩⣓⣐⣄⣬⣄⣾⣿
⣿⣿⣿⣿⣿⣿⣾⣿⣿⣿⣧⣦⣤⣦⣬⣭⣥⣶⣴⣷⣶⣷⣶⣶⣶⣾⣷⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
